# Steps to pull the project for development

1. Clone the repository.
    `git clone https://sandhyarakesh@bitbucket.org/huron-assets/staging-table.git`
2. Checkout to a dev branch.
    `git checkout dev`
3. Create a scratch org
    `sfdx force:org:create -s -f config/project-scratch-def.json -a myscratchorg`
4. Push the app to the scratch org
    `sfdx force:source:push`
5. Assign the permission set to the deafault user. For example:
    `sfdx force:user:permset:assign -n Huron_Staging_Table`
6. Open the scratch org:
    `sfdx force:org:open`
7. Validate the changes or make any required changes.
8. Pull the changes
    `sfdx force:source:pull`
9. Add and commit the changes to github
    `git add .`
    `git commit -m "Comments"`
10. Push the changes to the remote repository
    `git push origin dev`

## Read All About It

- [Salesforce Extensions Documentation](https://developer.salesforce.com/tools/vscode/)
- [Salesforce CLI Setup Guide](https://developer.salesforce.com/docs/atlas.en-us.sfdx_setup.meta/sfdx_setup/sfdx_setup_intro.htm)
- [Salesforce DX Developer Guide](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_intro.htm)
- [Salesforce CLI Command Reference](https://developer.salesforce.com/docs/atlas.en-us.sfdx_cli_reference.meta/sfdx_cli_reference/cli_reference.htm)
