@isTest(SeeAllData=false)
public class StagingTableHandlerTest {

    static testMethod void validateLoad() {
        test.startTest();
        
        addStagingSetup();
        addStagingRecords();
        addOtherData();
        
        Id batchJobId = Database.executeBatch(new StagingTableBatchScheduler(), 10);
        
        
        test.stopTest();
    }

    @isTest
    private static void testLeadDuplicatesContact () {
        addStagingSetup();

        //Create contact record inserted lead will match on
        Contact existingCon = new Contact(
            FirstName = 'Elvis',
            LastName  = 'Test2',
            Email     = 'etest@newmailservice.com',
            Phone     = '9791803459'
        );

        insert existingCon;

        Staging_Table__c leadTable = new Staging_Table__c(
            Objects_To_Upload__c    = 'Lead',
            Processing_Status__c    = 'Unprocessed',
            Processing_Schedule__c  = 'Immediate',
            First_Name__c           = 'Elvis',
            Last_Name__c            = 'Test2',
            Email__c                = 'etest@newmailservice.com',
            Phone__c                = '9791803459'
        );

        Test.startTest();
            insert leadTable;
        Test.stopTest();

        Staging_Table__c queriedTable = [SELECT Id, Contact__c, Lead__c FROM Staging_Table__c];
        Contact con = [SELECT Id, FirstName, LastName, Phone FROM Contact];
        /*Lead l = [SELECT Id, FirstName, LastName, Status FROM Lead];

        System.assertEquals(queriedTable.Lead__c, l.Id, 'Lead lookup on staging table expected to be same as existing lead');
        System.assertEquals(queriedTable.Contact__c, existingCon.Id, 'Contact lookup on staging table expected to be same as existing contact');
        System.assertEquals('9791803459', existingCon.Phone, 'Expecting phone to migrate from lead to contact, contact phone was ' + con.Phone);*/

    }

    @isTest
    private static void testContactDuplicatesLead () {

        addStagingSetup();

        //Create lead record inserted contact will match on
        Lead existingLead = new Lead(
            FirstName = 'John',
            LastName  = 'Test1',
            Email     = 'jtest@newmailservice.com',
            Phone     = '9701803459',
            Company   = 'TestCo'
        );

        insert existingLead;

        Staging_Table__c conTable = new Staging_Table__c(
            Objects_To_Upload__c    = 'Contact',
            Processing_Status__c    = 'Unprocessed',
            Processing_Schedule__c  = 'Immediate',
            First_Name__c           = 'John',
            Last_Name__c            = 'Test1',
            Email__c                = 'jtest@newmailservice.com'
        );

        Test.startTest();
            insert conTable;
        Test.stopTest();

        Staging_Table__c queriedTable = [SELECT Id, Contact__c, Lead__c FROM Staging_Table__c];
        /*Contact con = [SELECT Id, FirstName, LastName, Phone FROM Contact];
        Lead l = [SELECT Id, FirstName, LastName, Status FROM Lead];

        System.assertEquals(queriedTable.Lead__c, existingLead.Id, 'Lead lookup on staging table expected to be same as existing lead');
        System.assertEquals(queriedTable.Contact__c, con.Id, 'Contact lookup on staging table expected to be same as existing contact');
        //System.assertEquals('9701803459', con.Phone, 'Expecting phone to migrate from lead to contact, contact phone was ' + con.Phone);
        System.assertEquals(l.Status, 'Qualified', 'Expecting lead status to be \'Qualified Prospect\', was ' + l.Status);*/

    }
    
    @isTest
    private static void testHandler () {
        addStagingSetup();
        
        Test.startTest();
            stagingTableHandler sth = new stagingTableHandler(null);
            LeadStatus ls = sth.convertedStatus;
        Test.stopTest();

    }
    
    @isTest
    private static void testErrorHandler () {
        Test.startTest();
            addStagingSetup();
            addStagingRecords();
            addOtherData();
            
            String countryApiName = Contact.sObjectType.getDescribe().fields.getMap().containsKey('MailingCountryCode') ? 'MailingCountryCode' : 'MailingCountry';
            Id som_contact_id = [SELECT Id FROM Staging_Object_Mapping__c WHERE Target_Object_API_Name__c = 'Contact'].Id;
            insert new Staging_Field_Mapping__c(Object_Mapping__c = som_contact_id, Staging_Table_Field_API_Name__c = 'Country__c', Target_Field_API_Name__c = countryApiName, Active__c = TRUE);
            
            Id batchJobId = Database.executeBatch(new StagingTableBatchScheduler(), 10);
        Test.stopTest();

    }
    
    @isTest
    private static void testExternalIdHandler () {
        addStagingSetup();
        
        Id som_contact_id = [SELECT Id FROM Staging_Object_Mapping__c WHERE Target_Object_API_Name__c = 'Contact'].Id;
        database.insert(
            new Staging_Relationship_Mapping__c(
                Child_Object_Mapping__c = som_contact_id,
                Relationship_Field_API_Name__c = 'Account',
                External_ID_Field_API_Name__c = 'External_Id__c',
                Parent_Object_API_Name__c = 'Account',
                Related_to_Existing_Record__c = TRUE,
                Staging_Table_Field_API_Name__c = 'External_Id__c'
            )
        );
        
        Account acct = new Account(Name = 'Test', External_Id__c = 'testid');
        
        Staging_Table__c conTable = new Staging_Table__c(
            Objects_To_Upload__c    = 'Contact',
            Processing_Status__c    = 'Unprocessed',
            Processing_Schedule__c  = 'Immediate',
            First_Name__c           = 'John',
            Last_Name__c            = 'Test1',
            Email__c                = 'jtest@newmailservice.com',
            External_Id__c          = 'testid'
        );

        Test.startTest();
            insert conTable;
        Test.stopTest();
    }
    
    public static void addStagingRecords() {
        list<Staging_Table__c> stsToInsert = new list<Staging_Table__c>();
        
        Staging_Table__c st1 = new Staging_Table__c (
            Objects_To_Upload__c = 'Contact;Case',
            Processing_Status__c = 'Unprocessed',
            Last_Name__c = 'TestLast1',
            Email__c = 'test1@test.com',
            Processing_Schedule__c = 'On Schedule'
        );
        stsToInsert.add(st1);
        
        Staging_Table__c st2 = new Staging_Table__c(
            Objects_To_Upload__c = 'Contact;Case',
            Processing_Status__c = 'Unprocessed',
            Last_Name__c = 'TestLast1',
            Email__c = 'test1@test.com',
            Processing_Schedule__c = 'On Schedule'
        );
        stsToInsert.add(st2);
        
        Staging_Table__c st3 = new Staging_Table__c(
            Objects_To_Upload__c = 'Contact;Case',
            Processing_Status__c = 'Unprocessed',
            Last_Name__c = 'TestLast2',
            Email__c = 'test2@test.com',
            Opportunity_Name__c = 'Case Subject',
            Processing_Schedule__c = 'On Schedule'
        );
        stsToInsert.add(st3);
        
        Staging_Table__c st4 = new Staging_Table__c(
            Objects_To_Upload__c = 'Contact;Case',
            Processing_Status__c = 'Unprocessed',
            Last_Name__c = 'TestLast3',
            Email__c = 'test3@test.com',
            Processing_Schedule__c = 'On Schedule'
        );
        stsToInsert.add(st4);
        
        database.insert(stsToInsert);
        
        Staging_Table__c st5 = new Staging_Table__c(
            Objects_To_Upload__c = 'Contact',
            Processing_Status__c = 'Unprocessed',
            Last_Name__c = 'Test1',
            Email__c = 'test1@test.com',
            Processing_Schedule__c = 'On Schedule'
        );
        
        for (Integer i = 0; i < 20; i++)
            st5.Last_Name__c += 'Test1';
        insert st5;
    }
    
    public static void addOtherData() {
        Contact con = new Contact(
            FirstName = 'TestFirst',
            LastName = 'TestLast1',
            Email = 'test1@test.com'
        );
        insert con;
    }
    
    public static void addStagingSetup() {
        Staging_Object_Mapping__c som_contact = new Staging_Object_Mapping__c(
            Name = 'Contact',
            Target_Object_API_Name__c = 'Contact',
            Load_Order__c = 1
        );
        insert som_contact;
        
        Staging_Object_Mapping__c som_lead = new Staging_Object_Mapping__c(
            Name = 'Lead',
            Target_Object_API_Name__c = 'Lead',
            Load_Order__c = 2
        );
        insert som_lead;

        Staging_Object_Mapping__c som_case = new Staging_Object_Mapping__c(
            Name = 'Case',
            Target_Object_API_Name__c = 'Case',
            Load_Order__c = 3
        );
        insert som_case;
        
        // insert relationships
        list<Staging_Relationship_Mapping__c> relationships = new list<Staging_Relationship_Mapping__c>();
        relationships.add(
            new Staging_Relationship_Mapping__c(
                Parent_Object_Mapping__c = som_contact.Id,
                Child_Object_Mapping__c = som_case.Id,
                Relationship_Field_API_Name__c = 'ContactId'
            )
        );
        insert relationships;
        
        String countryApiName = Contact.sObjectType.getDescribe().fields.getMap().containsKey('MailingCountryCode') ? 'MailingCountryCode' : 'MailingCountry';
        // insert field mappings
        list<Staging_Field_Mapping__c> fields = new list<Staging_Field_Mapping__c>();
        fields.add(new Staging_Field_Mapping__c(Object_Mapping__c = som_contact.Id, Staging_Table_Field_API_Name__c = 'Last_Name__c', Target_Field_API_Name__c = 'LastName', Active__c = TRUE));
        fields.add(new Staging_Field_Mapping__c(Object_Mapping__c = som_contact.Id, Staging_Table_Field_API_Name__c = 'Email__c', Target_Field_API_Name__c = 'Email', Active__c = TRUE));
        fields.add(new Staging_Field_Mapping__c(Object_Mapping__c = som_contact.Id, Staging_Table_Field_API_Name__c = 'First_Name__c', Target_Field_API_Name__c = 'FirstName', Active__c = TRUE));
        fields.add(new Staging_Field_Mapping__c(Object_Mapping__c = som_contact.Id, Staging_Table_Field_API_Name__c = 'Phone__c', Target_Field_API_Name__c = 'Phone', Active__c = TRUE));
        fields.add(new Staging_Field_Mapping__c(Object_Mapping__c = som_lead.Id, Staging_Table_Field_API_Name__c = 'Last_Name__c', Target_Field_API_Name__c = 'LastName', Active__c = TRUE));
        fields.add(new Staging_Field_Mapping__c(Object_Mapping__c = som_lead.Id, Staging_Table_Field_API_Name__c = 'Email__c', Target_Field_API_Name__c = 'Email', Active__c = TRUE));
        fields.add(new Staging_Field_Mapping__c(Object_Mapping__c = som_lead.Id, Staging_Table_Field_API_Name__c = 'First_Name__c', Target_Field_API_Name__c = 'FirstName', Active__c = TRUE));
        fields.add(new Staging_Field_Mapping__c(Object_Mapping__c = som_lead.Id, Staging_Table_Field_API_Name__c = 'Phone__c', Target_Field_API_Name__c = 'Phone', Active__c = TRUE));
        fields.add(new Staging_Field_Mapping__c(Object_Mapping__c = som_lead.Id, Target_Field_API_Name__c = 'Company', Default_Value__c = 'TestCo', Active__c = TRUE));
        fields.add(new Staging_Field_Mapping__c(Object_Mapping__c = som_contact.Id, Staging_Table_Field_API_Name__c = 'Mailing_Country__c', Target_Field_API_Name__c = countryApiName, Active__c = TRUE));
        fields.add(new Staging_Field_Mapping__c(Object_Mapping__c = som_case.Id, Target_Field_API_Name__c = 'Origin', Default_Value__c = 'Web', Active__c = TRUE));
        fields.add(new Staging_Field_Mapping__c(Object_Mapping__c = som_case.Id, Target_Field_API_Name__c = 'Status', Default_Value__c = 'New', Active__c = TRUE));
        fields.add(new Staging_Field_Mapping__c(Object_Mapping__c = som_case.Id, Staging_Table_Field_API_Name__c = 'Opportunity_Name__c', Target_Field_API_Name__c = 'Subject', Active__c = TRUE));
        
        insert fields;
    }
}