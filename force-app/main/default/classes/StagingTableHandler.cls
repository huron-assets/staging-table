public class StagingTableHandler {
    
    public list<Staging_Table__c> records;
    
    public list<Staging_Object_Mapping__c> objs;
    public map<string, Staging_Object_Mapping__c> objMap;
    public map<string, list<Staging_Field_Mapping__c>> flds;
    public map<string, map<string, Staging_Relationship_Mapping__c>> rels;
    public boolean contactOwnerMapped;
    
    public map<integer, map<string, sObject>> itemsMap;
    public map<integer, list<sObject>> recsToInsert;
    public list<string> objectsHandled;
    public map<Staging_Table__c, id> dupeContactIdByStaging;

    public LeadStatus convertedStatus {
        get{
            if (this.convertedStatus == null)
                this.convertedStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];

            return this.convertedStatus;
        }
        set{}
    }

    public StagingTableHandler(list<Staging_Table__c> stagings){
        this.records = stagings;
    }
    
    public void processBatch() {
        contactOwnerMapped = false;
        
        // get order of the objects to load
        this.objs = [SELECT Id, Target_Object_API_Name__c, Load_Order__c, Update_Existing_Record__c FROM Staging_Object_Mapping__c ORDER BY Load_Order__c ASC];
        map<integer, string> loadOrder = new map<integer, string>();
        this.objMap = new map<string, Staging_Object_Mapping__c>();
        for (Staging_Object_Mapping__c som :this.objs) { 
            loadOrder.put(integer.valueOf(som.Load_Order__c), som.Target_Object_API_Name__c); 
            this.objMap.put(som.Target_Object_API_Name__c, som);
        }
        
        // get fields to map
        this.flds = new map<string, list<Staging_Field_Mapping__c>>();
        for (Staging_Field_Mapping__c sfm : [SELECT Id, Staging_Table_Field_API_Name__c, Target_Field_API_Name__c, Object_Mapping__r.Target_Object_API_Name__c, Default_Value__c, Active__c FROM Staging_Field_Mapping__c WHERE Active__c = TRUE]) {
            if (this.flds.containsKey(sfm.Object_Mapping__r.Target_Object_API_Name__c)) {
                this.flds.get(sfm.Object_Mapping__r.Target_Object_API_Name__c).add(sfm);
            } else {
                this.flds.put(sfm.Object_Mapping__r.Target_Object_API_Name__c, new list<Staging_Field_Mapping__c>{sfm});
            }
            
            if (sfm.Object_Mapping__r.Target_Object_API_Name__c.toLowerCase() == 'contact' && sfm.Target_Field_API_Name__c.toLowerCase() == 'ownerid') {
                this.contactOwnerMapped = TRUE;
            }
        }
        
        // get relationships to map
        this.rels = new map<string, map<string, Staging_Relationship_Mapping__c>>();
        map<string, list<Staging_Relationship_Mapping__c>> srmsByExternalID = new map<string, list<Staging_Relationship_Mapping__c>>();
        for (Staging_Relationship_Mapping__c srm : [SELECT Id, Child_Object_Mapping__r.Target_Object_API_Name__c, Parent_Object_Mapping__r.Target_Object_API_Name__c, Parent_Object_API_Name__c, Related_To_Existing_Record__c, Relationship_Field_API_Name__c, External_ID_Field_API_Name__c, Staging_Table_Field_API_Name__c FROM Staging_Relationship_Mapping__c]) {
            if (srm.Related_To_Existing_Record__c) {
                if (srmsByExternalID.containsKey(srm.Child_Object_Mapping__r.Target_Object_API_Name__c)) {
                    srmsByExternalID.get(srm.Child_Object_Mapping__r.Target_Object_API_Name__c).add(srm);
                } else {
                    srmsByExternalID.put(srm.Child_Object_Mapping__r.Target_Object_API_Name__c, new list<Staging_Relationship_Mapping__c>{srm});
                }
            } else {
                string relatedObject = srm.Parent_Object_Mapping__r.Target_Object_API_Name__c;
                if (this.rels.containsKey(srm.Child_Object_Mapping__r.Target_Object_API_Name__c)) {
                    this.rels.get(srm.Child_Object_Mapping__r.Target_Object_API_Name__c).put(relatedObject, srm);
                } else {
                    this.rels.put(srm.Child_Object_Mapping__r.Target_Object_API_Name__c, new map<string, Staging_Relationship_Mapping__c>{relatedObject=>srm});
                }
                
                if (srm.Parent_Object_Mapping__r.Target_Object_API_Name__c.toLowerCase() == 'contact' && srm.Relationship_Field_API_Name__c.toLowerCase() == 'ownerid') {
                    this.contactOwnerMapped = TRUE;
                }
            }
        }
        
        this.recsToInsert = new map<integer, list<sObject>>();
        this.itemsMap = new map<integer, map<string, sObject>>();
        
        integer i = 0;
        integer maxLoadNum = 0;
        for (Staging_Table__c staging : this.records) {
            this.itemsMap.put(i, new map<string, sObject>{'Staging_Table__c'=>staging});
            try {
                for (Staging_Object_Mapping__c som :this.objs) { // str = object API Name
                    string str = som.Target_Object_API_Name__c;
                    if (staging.Object_API_Names__c.contains(str)) {
                        system.debug('--- object: ' + str);
                        Schema.SObjectType recType = Schema.getGlobalDescribe().get(str);
                        
                        // get all fields for this object
                        map<String, Schema.SObjectField> fieldMap = recType.getDescribe().fields.getMap();
                        // create instance
                        sObject rec = recType.newSObject();
                        
                        // populate any field values, if applicable
                        if (this.flds.containsKey(str)) { 
                            for (Staging_Field_Mapping__c sfm : this.flds.get(str)) {
                                if (string.isBlank(sfm.Staging_Table_Field_API_Name__c)) {
                                    rec = setDefaultValue(rec, sfm, fieldMap);
                                } else {
                                    
                                    if (staging.get(sfm.Staging_Table_Field_API_Name__c) != null) {
                                        if (!string.isBlank(string.valueOf(staging.get(sfm.Staging_Table_Field_API_Name__c)))) {
                                            system.debug('--- field: ' + sfm.Target_Field_API_Name__c + ', value: ' + staging.get(sfm.Staging_Table_Field_API_Name__c));
                                            rec.put(sfm.Target_Field_API_Name__c, staging.get(sfm.Staging_Table_Field_API_Name__c));
                                        }
                                    }
                                    
                                    // populate with the default value if the staging table field value is blank
                                    if (staging.get(sfm.Staging_Table_Field_API_Name__c) == null) {
                                        rec = setDefaultValue(rec, sfm, fieldMap);
                                    }
                                }
                            }
                        }
                        
                        // populate any relationship values by external ID
                        if (srmsByExternalID.containsKey(str)) { 
                            for (Staging_Relationship_Mapping__c srm : srmsByExternalID.get(str)) {
                                if (!string.isBlank((string)staging.get(srm.Staging_Table_Field_API_Name__c))) {
                                    //system.debug('--- external id relationship: ' + srm.Relationship_Field_API_Name__c);
                                    
                                    Schema.SObjectType relRecType = Schema.getGlobalDescribe().get(srm.Parent_Object_API_Name__c);
                                    sObject relatedRecord = relRecType.newSObject();
                                    relatedRecord.put(srm.External_ID_Field_API_Name__c, staging.get(srm.Staging_Table_Field_API_Name__c));
                                    
                                    rec.putSObject(srm.Relationship_Field_API_Name__c, relatedRecord);
                                }
                            }
                        }
                        
                        // add record to be created
                        integer j = integer.valueOf(som.Load_Order__c);
                        if (this.recsToInsert.containsKey(j)) {
                            this.recsToInsert.get(j).add(rec);
                        } else {
                            this.recsToInsert.put(j, new list<sObject>{rec});
                        }
                        
                        // collect the record for reference later
                        this.itemsMap.get(i).put(str, rec);
                        // set maxLoadNum
                        if (j > maxLoadNum) maxLoadNum = j;
                    }
                }
                staging.Audit_Reason__c = '';
                staging.Processing_Outcome__c = 'Instantiated';
            } catch (exception ex) {
                system.debug('--- error message (processBatch): ' + ex.getMessage());
                staging.Processing_Outcome__c = 'Error';
                staging.Audit_Reason__c = getAuditReason(staging.Audit_Reason__c, ex.getMessage()); //ex.getMessage() + '; ';
            }
            staging.Processing_Status__c = 'Processed';
            staging.Last_Processed_Date__c = system.now();
            i++;
        }
        
        system.debug('--- recsToInsert: ' + this.recsToInsert);
        this.objectsHandled = new list<string>();
        for (integer j=0; j<=maxLoadNum; j++) {
            if (this.recsToInsert.containsKey(j)) {
                if (loadOrder.containsKey(j)) {
                    list<sObject> sobjs = this.recsToInsert.get(j);
                    system.debug('--- j: ' + j + ', recs: ' + sobjs);
                    
                    string thisObject = loadOrder.get(j); // api name of this object
                    boolean doUpdate = this.objMap.get(thisObject).Update_Existing_Record__c;
                    
                    // create these records
                    saveRecords(sobjs, true, 1, thisObject, doUpdate);
                    system.debug('Im trying to make a ' + thisObject + ' with ' + sobjs);
                    
                    // set the relationships on this object
                    setRelationships(thisObject);
                    
                    // add this object to the list of objects that have already been handled - todo: remove?
                    this.objectsHandled.add(thisObject);
                }
            }
        }
        
        // update staging table records
        Database.SaveResult[] stagingSave = Database.update(this.records, false);
    }
    
    
    public void saveRecords(list<sObject> itemsToHandle, boolean isInsert, integer iteration, string sobjType, boolean doUpdate) {
        if (iteration <= 3) {
            doSaveRecords(itemsToHandle, isInsert, iteration, sobjType, doUpdate);
        }
    }
    
    public void saveRecords(map<Id, sObject> itemsToHandleMap, boolean isInsert, integer iteration, string sobjType, boolean doUpdate) {
        if (iteration <= 3) {
            list<sObject> itemsToHandle = itemsToHandleMap.values();
            doSaveRecords(itemsToHandle, isInsert, iteration, sobjType, doUpdate);
        }
    }
    
    public void doSaveRecords(list<sObject> itemsToHandle, boolean isInsert, integer iteration, string sobjType, boolean doUpdate) {

        Database.SaveResult[] saveResults;
        
        if (sobjType == 'Case') {
            // creating the DMLOptions for "Assign using active assignment rules" checkbox
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.useDefaultRule = true;

            
            for (sObject sobj : itemsToHandle) {
                if (isInsert) {
                    //Setting the DMLOption on Case instance
                    sobj.setOptions(dmlOpts);
                } else {
                    dmlOpts.assignmentRuleHeader.assignmentRuleId = null;
                    dmlOpts.assignmentRuleHeader.useDefaultRule = false;
                    
                    sobj.setOptions(dmlOpts);
                }
            }
        }
        
        if (isInsert) saveResults = Database.insert(itemsToHandle, false);
        else saveResults = Database.update(itemsToHandle, false);
        
        map<Id, sObject> itemsToUpdate = new map<Id, sObject>();
        map<Id, sObject> itemsToUpdateByLeadId = new map<Id, sObject>();
        map<Id, sObject> itemsToUpdateByContactId = new map<Id, sObject>();
        map<Id, Staging_Table__c> stagingByAltId = new map<Id, Staging_Table__c>();

        for (Integer i = 0; i < saveResults.size(); i++) { // iterate through all records
            Database.SaveResult sr = saveResults[i];
            sObject itemToUpdate = itemsToHandle[i];
            
            string recordId = '';
            string altRecordId = ''; //If a Contact is returned as a duplicate of a lead or vice versa, perform lead conversion or update contact record with lead information
            
            if (!sr.isSuccess()) { // if there was an error, determine if it was caused by a duplicate record
                for (Database.Error error : sr.getErrors()) {
                    system.debug('--- error (database): ' + error.getMessage());
                    
                    // process only duplicates and not errors (e.g. validation errors)
                    if (error instanceof database.DuplicateError) { 
                        Database.DuplicateError duplicateError = (Database.DuplicateError)error;
                        Datacloud.MatchResult matchResult = duplicateError.getDuplicateResult().getMatchResults()[0];
                        Datacloud.MatchRecord[] matchRecords = matchResult.getMatchRecords();
                        
                        // capture id of record that duplicate error is thrown on. If there are more than one, match on the first duplicate found.
                        for (Datacloud.MatchRecord potentialMatch : matchRecords)
                            if (Schema.getGlobalDescribe().get(sobjType).getDescribe() == potentialMatch.getRecord().getSObjectType().getDescribe()) {
                                system.debug(potentialMatch.getRecord());
                                recordId = matchRecords[0].getRecord().Id;
                                break;
                            
                            //If a contact duplicates an inserted lead or vice versa, need to update the contact with lead information or convert the lead
                            } else if ((sobjType == 'Lead' && potentialMatch.getRecord().getSObjectType().getDescribe().getName() == 'Contact') || (sobjType == 'Contact' && potentialMatch.getRecord().getSObjectType().getDescribe().getName() == 'Lead')) {
                                altRecordId = potentialMatch.getRecord().Id;

                            }
                        
                        // set id of record to allow update instead
                        if (!string.isBlank(recordId)) {
                            if (doUpdate) {

                                itemToUpdate.Id = recordId;
                                itemsToUpdate.put(recordId, itemToUpdate);

                            } else {
                                // create new instance to avoid updating any field values
                                Schema.SObjectType recType = Schema.getGlobalDescribe().get(sobjType);
                                sObject existingItemToUpdate = recType.newSObject();
                                
                                existingItemToUpdate.Id = recordId;
                                itemsToUpdate.put(recordId, existingItemToUpdate);

                            }
                        } else if (!string.isBlank(altRecordId)) {
                            if (sobjType == 'Lead') {
                                //If a lead is being inserted but a contact exists, we want to update the existing contact instead   
                                //this.dupeContactIdByStaging.put(this.records[i], altRecordId);
                                itemsToUpdateByContactId.put(altRecordId, itemToUpdate);
                                stagingByAltId.put(altRecordId,this.records[i]);

                            } else if (sobjType == 'Contact') {
                                //If a Contact is being inserted and we find a matching lead, convert the lead and then update those converted contacts
                                itemsToUpdateByLeadId.put(altRecordId, itemToUpdate);
                                stagingByAltId.put(altRecordId,this.records[i]);

                            }
                        }
                    } else {
                        string errorMessage = error.getMessage();
                        // if the duplicate error is thrown on a field, then proceed using the duplicate record id
                        if (errorMessage.contains('duplicate') && errorMessage.contains('id: ')) {
                            recordId = errorMessage.substringAfter('id: ' );
                            
                            // set id of record to allow update instead
                            if (!string.isBlank(recordId)) {
                                //itemToUpdate.Id = recordId;
                                //itemsToUpdate.put(recordId, itemToUpdate);
                                
                                if (doUpdate) {
                                    itemToUpdate.Id = recordId;
                                    itemsToUpdate.put(recordId, itemToUpdate);

                                } else {
                                    // create new instance to avoid updating any field values
                                    Schema.SObjectType recType = Schema.getGlobalDescribe().get(sobjType);
                                    sObject existingItemToUpdate = recType.newSObject();
                                    
                                    existingItemToUpdate.Id = recordId;
                                    itemsToUpdate.put(recordId, existingItemToUpdate);

                                }
                            }
                        } else {
                            // update audit reason on the staging table record
                            this.records[i].Audit_Reason__c = getAuditReason(this.records[i].Audit_Reason__c, errorMessage); //error.getMessage() + '; ';
                            if (this.records[i].Processing_Outcome__c == 'Success') {
                                this.records[i].Processing_Outcome__c = 'Partial Insert';

                            } else {
                                this.records[i].Processing_Outcome__c = 'Error';

                            }
                        }
                    }
                }
            } else {
                if (sobjType == 'Contact' || sobjType == 'Lead') {
                    recordId = sr.getId();
                
                }
                if (this.records[i].Processing_Outcome__c == 'Instantiated') {
                    this.records[i].Processing_Outcome__c = 'Success';
                
                }
            }
            
            // set contact or lead ID on the staging table record
            if (isInsert && !string.isBlank(recordId)) {
                if (sobjType == 'Contact') {
                    this.records[i].Contact__c = recordId;
                } else if (sobjType == 'Lead') {
                    this.records[i].Lead__c = recordId;
                }
            }
            recordId = '';
        }

        //If any contacts were matched to a lead, convert those lead to update the existing contact
        if (!itemsToUpdateByLeadId.isEmpty()) {

            List<List<Database.leadConvert>> leadConverts = new List<List<Database.leadConvert>>(); //We can only convert 100 leads at once, so put into a list 

            for (Id dupeLeadId : itemsToUpdateByLeadId.keySet()) {
                Id ownerId;
                if (this.contactOwnerMapped) {
                    ownerId = (Id)itemsToUpdateByLeadId.get(dupeLeadId).get('OwnerId');
                    system.debug('--- found contact owner id: ' + ownerId);
                } 
                
                if (string.isBlank(ownerId)) {
                    ownerId = UserInfo.getUserId();
                }
                system.debug('--- lead convert owner id: ' + ownerId);
                
                if (leadConverts.isEmpty() || leadConverts[leadConverts.size()-1].size() == 100) {
                    List<Database.leadConvert> currentList = new List<Database.leadConvert>();
                    leadConverts.add(currentList);
                }
                Database.LeadConvert lc = new Database.LeadConvert();

                lc.setLeadId(dupeLeadId);
                lc.setOwnerId(ownerId);
                lc.setConvertedStatus(this.convertedStatus.MasterLabel);
                lc.setDoNotCreateOpportunity(true);

                leadConverts[leadConverts.size()-1].add(lc);
            }

            for (List<Database.leadConvert> lcList : leadConverts) {
                
                List<Database.LeadConvertResult> lcrs = Database.convertLead(lcList);

                for (Integer i=0; i < lcrs.size(); i++) {

                    Database.LeadConvertResult lcr = lcrs[i];
                    Database.LeadConvert lc = lcList[i];
                    
                    Staging_Table__c staging = stagingByAltId.get(lcr.getLeadId());                

                    if (lcr.isSuccess()) {
                        itemsToUpdateByLeadId.get(lcr.getLeadId()).Id = lcr.getContactId();
                        staging.Contact__c = lcr.getContactId();
                        staging.Lead__c = lcr.getLeadId();
                        Contact c = (Contact)itemsToUpdateByLeadId.get(lcr.getLeadId());
                        c.Id = lcr.getContactId();
                        itemsToUpdate.put(c.Id, c);
                        if (staging.Processing_Outcome__c == 'Instantiated') 
                            staging.Processing_Outcome__c = 'Success';            
                    } else {
                        
                        String leadError = 'Found Lead duplicating Contact (Id: ' + lcr.getLeadId() +'), Lead could not be converted. Error was: ' + lcr.getErrors()[0].getMessage();
                        staging.Audit_Reason__c = getAuditReason(staging.Audit_Reason__c, leadError);
                        
                        if (staging.Processing_Outcome__c == 'Success') {
                            staging.Processing_Outcome__c = 'Partial Insert';

                        } else {
                            staging.Processing_Outcome__c = 'Error';

                        }                    
                    }
                }
            }
        }

        if (!itemsToUpdateByContactId.isEmpty()) {
            List<Lead> newLeads = new List<Lead>();                    
            List<List<Database.leadConvert>> leadConverts = new List<List<Database.leadConvert>>(); //We can only convert 100 leads at once, so put into a list 
            List<Staging_Table__c> stagings = new List<Staging_Table__c>();
            Map<Id, Id> accIdByConIds = new Map<Id, Id>();
            Map<Id, Id> ownerIdsByConIds = new Map<Id, Id>();
            List<Id> conIds = new List<Id>();

            for (Contact c  : [SELECT Id, AccountId, OwnerId FROM Contact WHERE Id IN :itemsToUpdateByContactId.keySet()]) {
                accIdByConIds.put(c.Id, c.AccountId);
                ownerIdsByConIds.put(c.Id, c.OwnerId);
            }

            for (String conId : itemsToUpdateByContactId.keySet()){
                newLeads.add((Lead)itemsToUpdateByContactId.get(conId));
                stagings.add(stagingByAltId.get(conId));
                conIds.add(conId);
            }

            Database.DMLOptions allowDupes = new Database.DMLOptions();
            allowDupes.DuplicateRuleHeader.allowSave = true;
            allowDupes.optAllOrNone = false;

            List<Database.SaveResult> srs = Database.insert(newLeads, allowDupes);

            for (Integer i=0; i < srs.size(); i++) {
                if (!srs[i].isSuccess()) {

                    String leadError = 'Found Contact duplicating Lead (Id: ' + conIds[i] +'), failed to create lead for immediate conversion. Error was: ' + srs[i].getErrors()[0].getMessage();
                    stagings[i].Audit_Reason__c = getAuditReason(stagings[i].Audit_Reason__c, leadError);

                    if (stagings[i].Processing_Outcome__c == 'Success') {
                        stagings[i].Processing_Outcome__c = 'Partial Insert';

                    } else {
                        stagings[i].Processing_Outcome__c = 'Error';

                    }    
                }
            }

            for (String conId : itemsToUpdateByContactId.keySet()) {
                if (leadConverts.isEmpty() || leadConverts[leadConverts.size()-1].size() == 100) {
                    List<Database.leadConvert> currentList = new List<Database.leadConvert>();
                    leadConverts.add(currentList);
                
                }

                Database.LeadConvert lc = new Database.LeadConvert();
                if (itemsToUpdateByContactId.get(conId).Id != null) {
                    lc.setLeadId(itemsToUpdateByContactId.get(conId).Id);
                    lc.setConvertedStatus(Test.isRunningTest() ? 'Closed - Converted' : this.convertedStatus.MasterLabel);
                    lc.setContactId(conId);
                    lc.setAccountId(accIdByConIds.get(conId));
                    //lc.setOwnerId(ownerIdsByConIds.get(conId));
                    lc.setDoNotCreateOpportunity(true);
                    leadConverts[leadConverts.size()-1].add(lc);
                }
            }

            for (List<Database.leadConvert> lcList : leadConverts) {

                List<Database.LeadConvertResult> lcrs = Database.convertLead(lcList);

                for (Integer i=0; i < lcrs.size(); i++) {

                    Database.LeadConvertResult lcr = lcrs[i];
                    Database.LeadConvert lc = lcList[i];

                    Staging_Table__c staging = stagingByAltId.get(lc.getContactId());
    
                    if (lcr.isSuccess()) {
                        staging.Contact__c = lcr.getContactId();
                        staging.Lead__c = lcr.getLeadId();
                        if (staging.Processing_Outcome__c == 'Instantiated') 
                            staging.Processing_Outcome__c = 'Success'; 
                    }  else {

                        String leadError = 'Found Contact duplicating Lead (Id: ' + lc.getContactId() +'), Lead could not be converted. Error was: ' + lcr.getErrors()[0].getMessage();
                        staging.Audit_Reason__c = getAuditReason(staging.Audit_Reason__c, leadError);

                        if (staging.Processing_Outcome__c == 'Success') {
                            staging.Processing_Outcome__c = 'Partial Insert';

                        } else {
                            staging.Processing_Outcome__c = 'Error';

                        }                    
                    }
                }
            }
        }
        
        // if any duplicates were detected, update records instead
        if (itemsToUpdate.size() > 0) saveRecords(itemsToUpdate, false, iteration+1, sobjType, doUpdate);

    }
    
    public void setRelationships(string relationshipObject) { // relationshipObject = object api name of records being updated
        system.debug('--- relationshipObject: ' + relationshipObject);
        for (Integer i : this.itemsMap.keySet()) {
            //sObject staging = itemsMap.get(i).get('Staging_Table__c');
            Staging_Table__c staging = this.records[i];
            for (Staging_Object_Mapping__c som :this.objs) { // str = object API Name
                try {
                    string str = som.Target_Object_API_Name__c;
                    if (((string)staging.get('Object_API_Names__c')).contains(str) && !this.objectsHandled.contains(str)) {
                        system.debug('--- object: ' + str);
                        sObject rec = this.itemsMap.get(i).get(str);
                        
                        if (this.rels.containsKey(str)) { // populate any relationship values, if applicable
                            if (this.rels.get(str).containsKey(relationshipObject)) {
                                Staging_Relationship_Mapping__c srm = this.rels.get(str).get(relationshipObject);
                                system.debug('--- relationship: ' + srm.Relationship_Field_API_Name__c);
                                
                                if (!srm.Related_To_Existing_Record__c) {
                                    system.debug('--- relationship field name: ' + srm.Relationship_Field_API_Name__c + ', relationship field value: ' + this.itemsMap.get(i).get(srm.Parent_Object_Mapping__r.Target_Object_API_Name__c).get('Id'));
                                    rec.put(srm.Relationship_Field_API_Name__c, this.itemsMap.get(i).get(srm.Parent_Object_Mapping__r.Target_Object_API_Name__c).get('Id'));
                                }
                            }
                        }
                    }
                } catch (exception ex) {
                    system.debug('--- error message (setRelationships): ' + ex.getMessage());
                    staging.put('Audit_Reason__c', getAuditReason((string)staging.get('Audit_Reason__c'), ex.getMessage())); //staging.get('Audit_Reason__c') + '; ' + ex.getMessage());
                    staging.put('Processing_Outcome__c', 'Error');
                }
                
            }
        }
    }
    
    public static sObject setDefaultValue(sObject rec, Staging_Field_Mapping__c sfm, map<String, Schema.SObjectField> fieldMap) {
        string fieldType = string.valueOf(fieldMap.get(sfm.Target_Field_API_Name__c).getDescribe().getType());
        //system.debug('--- fieldtype: ' + fieldType);
        if(!string.isBlank(sfm.Default_Value__c)) {
            if (fieldType == 'Date') {
                if (!string.isEmpty(sfm.Default_Value__c)) {
                    rec.put(sfm.Target_Field_API_Name__c, date.parse(sfm.Default_Value__c));
                }
            } else if (sfm.Default_Value__c.toLowerCase() == 'true' || sfm.Default_Value__c.toLowerCase() == 'false') {
                if (sfm.Default_Value__c.toLowerCase() == 'true') {
                    rec.put(sfm.Target_Field_API_Name__c, TRUE);
                } else {
                    rec.put(sfm.Target_Field_API_Name__c, FALSE);
                }
            } else {
                rec.put(sfm.Target_Field_API_Name__c, sfm.Default_Value__c);
            }
        }
        return rec;
    }
    
    public static string getAuditReason(string auditReason, string errorMsg) {
        auditReason += errorMsg + '; ';
        if (auditReason.length() <= 225) {
            return auditReason;
        } else {
            return auditReason.substring(0, 224);
        }
    }

    
}