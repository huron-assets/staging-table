public class StagingTableTriggerHandler {
    
    public static void processRecords(list<Staging_Table__c> newList) {
        list<Id> idsToProcess = new list<Id>();
        for (Staging_Table__c tbl : newList) {
            if (tbl.Processing_Status__c == 'Unprocessed' && tbl.Processing_Schedule__c == 'Immediate') {
                idsToProcess.add(tbl.Id);
            }
        }
        
        if (!idsToProcess.isEmpty()) {
            DescribeSObjectResult describeResult = Staging_Table__c.getSObjectType().getDescribe();
            List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );
            string query = 'SELECT ' + String.join( fieldNames, ',' ) + ' FROM Staging_Table__c WHERE Id IN :idsToProcess';
            
            list<Staging_Table__c> recordsToProcess = database.query(query);
            
            stagingTableHandler sth = new stagingTableHandler(recordsToProcess);
            sth.processBatch();
        }
    }
    
}