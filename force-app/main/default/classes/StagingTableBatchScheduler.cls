global class StagingTableBatchScheduler implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful, Schedulable {
    
    global void execute(SchedulableContext sc) {
        Id batchJobId = Database.executeBatch(new StagingTableBatchScheduler(), 100);
        system.debug('--- batch job id: ' + batchJobId);
    }
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        DescribeSObjectResult describeResult = Staging_Table__c.getSObjectType().getDescribe();
        List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );
        
        string criteria = 'Unprocessed';
        string query = 'SELECT ' + String.join( fieldNames, ',' ) + ' FROM Staging_Table__c WHERE Processing_Status__c = :criteria';
        
        //string query = 'SELECT ' + String.join( fieldNames, ',' ) + ' FROM Staging_Table__c WHERE Id = :criteria';
        system.debug('--- query: ' + query);
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext context, Staging_Table__c[] records) {
        stagingTableHandler sth = new stagingTableHandler(records);
        sth.processBatch();
    }
    
    global void finish(Database.BatchableContext BC){
        system.debug('--- batch complete');
    }
    
}