trigger StagingTableTrigger on Staging_Table__c (after insert, after update) {
    
    if (Trigger.isAfter) {
        if (Trigger.isInsert) StagingTableTriggerHandler.processRecords(Trigger.new);
        if (Trigger.isUpdate) StagingTableTriggerHandler.processRecords(Trigger.new);
    }
    
}